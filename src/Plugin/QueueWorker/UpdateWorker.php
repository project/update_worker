<?php

namespace Drupal\update_worker\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sends a message with sparkpost.
 *
 * @QueueWorker(
 *   id = "update_worker",
 *   title = @Translation("Run arbitrary commands as queue items"),
 *   cron = {}
 * )
 */
class UpdateWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  const QUEUE_NAME = 'update_worker';

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = new static($configuration, $plugin_id, $plugin_definition);
    $plugin->moduleHandler = $container->get('module_handler');
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    // Load all files with update_worker.php in the name. This is a recommended
    // location for update worker callbacks to live. The file won't be
    // autoaded, so it won't affect site's performance until the queue is
    // executed.
    $this->moduleHandler->loadAllIncludes('update_worker.php');
    $callback = $data['callback'];
    $arguments = $data['arguments'];
    if (is_callable($callback)) {
      call_user_func_array($callback, $arguments);
    }
  }

}
