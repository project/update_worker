<?php

namespace Drupal\Tests\update_worker\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\update_worker\Plugin\QueueWorker\UpdateWorker;

/**
 * Tests the basic functionality of the module.
 *
 * @group update_worker
 */
class UpdateWorkerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'update_worker',
    'update_worker_test',
  ];

  /**
   * Test that our queue is run, and the callback called.
   */
  public function testUpdateWorkerCalled() {
    // This value, for those who wonder, is an episode of the X-files.
    $my_test_value = "Clyde Bruckman's Final Repose";
    /** @var \Drupal\Core\State\StateInterface $state */
    $state = $this->container->get('state');
    $state_key = 'update_worker_test_state_key';
    // So initially, this value should be empty.
    $this->assertEmpty($state->get($state_key));
    $queue = $this->container->get('queue')->get(UpdateWorker::QUEUE_NAME);
    $queue->createItem([
      // We are creating an item that will update this value in this callback.
      'callback' => 'update_worker_test_update_state_value',
      'arguments' => [
        $my_test_value,
      ],
    ]);
    // Run cron, which in turn will run the queue item.
    /** @var \Drupal\Core\CronInterface $cron */
    $cron = $this->container->get('cron');
    $cron->run();
    // Now the state value should have been updated by our test implementation.
    $this->assertEquals($state->get($state_key), $my_test_value);
  }

  /**
   * Test if callbacks are loaded from the designated location.
   */
  public function testDesignatedCallbackLocation() {
    // This value, for those who wonder, is an episode of Friends.
    $my_test_value = "The One Where Ross Can't Flirt";
    /** @var \Drupal\Core\State\StateInterface $state */
    $state = $this->container->get('state');
    $state_key = 'update_worker_test_state_key';
    // So initially, this value should be empty.
    $this->assertEmpty($state->get($state_key));
    $queue = $this->container->get('queue')->get(UpdateWorker::QUEUE_NAME);
    $queue->createItem([
      // We are creating an item that will update this value in this callback.
      'callback' => 'update_worker_test_callback_in_designated_location',
      'arguments' => [
        $my_test_value,
      ],
    ]);
    // Run cron, which in turn will run the queue item.
    /** @var \Drupal\Core\CronInterface $cron */
    $cron = $this->container->get('cron');
    $cron->run();
    // Now the state value should have been updated by our test implementation.
    $this->assertEquals($state->get($state_key), $my_test_value);
  }

}
