<?php

/**
 * @file
 * Hooks for this test module.
 */

/**
 * Function that will be run by the queue worker.
 */
function update_worker_test_callback_in_designated_location($value) {
  \Drupal::state()->set('update_worker_test_state_key', $value);
}
